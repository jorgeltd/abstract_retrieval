"""
Module for sqlAlchemy database connection
"""

from sqlalchemy import Engine, create_engine, URL
from sqlalchemy.orm import Session

from settings import settings


class Singleton(type):
    """
    Metaclass to create singleton
    """

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class DBConnection(metaclass=Singleton):
    """
    Class for hosting database connection/sessions
    """

    def __init__(self, url: URL):
        self.engine: Engine = create_engine(url)

    def close(self) -> None:
        """
        Close the connection to database
        """
        self.engine.dispose()

    def session(self) -> Session:
        """
        Create a SQLAlchemy session to make database query's
        """
        return Session(self.engine)


sdi_db_conn = DBConnection(settings.get_db_url())
