"""
Module for general use API codes
"""
from typing import Final

INTERNAL_SERVER_ERROR: Final = "INTERNAL_SERVER_ERROR"
