"""
Schema definitions for document search module
"""

from typing import Union

from benedict import benedict
from pydantic import BaseModel, constr


class DoiBody(BaseModel):
    """
    Request body with doi parameter
    """

    doi: constr(
        regex=r"^10.\d{4,9}\/([a-zA-Z0-9]|[-_:;()\/\.])+$",
        to_lower=True,
        strip_whitespace=True,
    )

    class Config:
        """
        Documentation example
        """

        schema_extra = {
            "example": {
                "doi": "10.5194/tc-14-2545-2020",
            }
        }


class PublicationDate(BaseModel):
    """
    Schema for publication date
    """

    year: int
    month: Union[int, None]
    day: Union[int, None]


class ScopusAffiliation(BaseModel):
    """
    Schema for affiliation information parsed from Scopus API
    """

    id: str
    city: str
    country: str
    name: str
    href: str


class ScopusAuthor(BaseModel):
    """
    Schema for author information parsed from Scopus API
    """

    name: Union[str, None]
    surname: str
    order: int
    indexed_name: str
    initials: str
    affiliations: list[ScopusAffiliation]


class ScopusAbstract(BaseModel):
    """
    Schema for publication information parsed from Scopus API
    """

    journal: str
    publisher: str
    document_type: str
    title: str
    start_page: Union[int, None]
    end_page: Union[int, None]
    issn: Union[list[str], None]
    volume: Union[str, None]
    issue: Union[str, None]
    abstract: Union[str, None]
    doi: str
    keywords: set[str]
    date: PublicationDate
    authors: list[ScopusAuthor]


class ScopusAbstractRetrival(BaseModel):
    """
    Schema for scopus abstract retrival API call response
    """

    status_code: int
    data: Union[benedict, None] = None
    error_code: Union[str, None] = None

    def error_detail(self) -> dict:
        """
        Get status code and error code from failed API call
        :return: dictionary with status_code and error_code
        """
        return {"status_code": self.status_code, "error_code": self.error_code}
