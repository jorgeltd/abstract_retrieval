"""
Module for document search use API codes, External APIs, etc
"""

from typing import Final

SCOPUS_ENDPOINTS: Final = {
    "get_abstract_by_doi": "https://api.elsevier.com/content/abstract/doi"
}
