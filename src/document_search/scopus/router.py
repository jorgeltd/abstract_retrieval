"""
Temp module for testing scopus services
"""
from fastapi import APIRouter, HTTPException

from src.document_search.scopus.schemas import DoiBody, ScopusAbstract
from src.document_search.scopus.service import scopus_abstract_retrieval
from src.document_search.scopus.utils import parse_scopus_abstract

router = APIRouter()


@router.get("/scopus/abstract_by_doi", tags=["scopus"])
async def get_scopus_abstract_from_doi(document: DoiBody) -> ScopusAbstract:
    """
    Search document on Scopus db by doi
    """
    results = await scopus_abstract_retrieval(document.doi)

    # error in query
    if results.status_code != 200:
        raise HTTPException(
            status_code=results.status_code, detail=results.error_detail()
        )

    scopus_abstract = parse_scopus_abstract(results.data)

    return scopus_abstract
