"""
Module for business logic of document search module
"""


import ujson
from aiohttp import ClientSession
from benedict import benedict

from src.document_search.scopus.constants import SCOPUS_ENDPOINTS
from src.document_search.scopus.schemas import ScopusAbstractRetrival
from src.document_search.scopus.utils import get_scopus_error_code
from src.settings import settings


async def scopus_abstract_retrieval(doi: str) -> ScopusAbstractRetrival:
    """
    Make API call to Scopus get abstract by doi
    :param doi: doi string
    :return: Publication data formatted as ScopusAbstractRetrival schema
    """
    async with ScopusClient() as client:
        async with client.get(
            f"{SCOPUS_ENDPOINTS['get_abstract_by_doi']}/{doi}"
        ) as response:
            data = await response.json()
            status_code = response.status

    if status_code == 200:
        return ScopusAbstractRetrival(
            data=benedict(data["abstracts-retrieval-response"]), status_code=status_code
        )

    # Get failed api call error code
    return get_scopus_error_code(status_code, data)


class ScopusClient:
    """
    Client for making requests to Scopus API
    """

    api_key: str = settings.scopus_key
    session = None

    async def __aenter__(self):
        headers = {"Accept": "application/json", "X-ELS-APIKey": f"{self.api_key}"}
        self.session = ClientSession(
            json_serialize=ujson.dumps,
            headers=headers,
        )
        return self.session

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.session.close()
