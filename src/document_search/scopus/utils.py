"""
Module with functions for parsing data/results from Scopus API
"""

from benedict import benedict

from src.document_search.scopus.schemas import (
    PublicationDate,
    ScopusAbstract,
    ScopusAbstractRetrival,
    ScopusAffiliation,
    ScopusAuthor,
)


def format_issn(issn: str) -> str:
    """
    Convert add dash to ISSN code
    """
    assert len(issn) == 8
    return issn[:4] + "-" + issn[4:]


def parse_affiliations(affiliations: list[dict]) -> dict[str, ScopusAffiliation]:
    """
    Parse publication affiliations data from Scopus API
    :param affiliations: list of dictionaries with affiliation data
    :return dictionary with affiliations information parsed to schema
    """
    parsed_affiliation = {}

    for affiliation in affiliations:
        scopus_id = affiliation["@id"]

        parsed_affiliation[scopus_id] = ScopusAffiliation(
            id=scopus_id,
            city=affiliation["affiliation-city"],
            country=affiliation["affiliation-country"],
            name=affiliation["affilname"],
            href=affiliation["@href"],
        )

    return parsed_affiliation


def parse_authors(
    authors: list[benedict], affiliations: dict[str, ScopusAffiliation]
) -> list[ScopusAuthor]:
    """
    Parse publication authors data from Scopus API
    :param authors: list of dictionaries with authors information
    :param affiliations: dictionary with [scopus_id, ScopusAffiliation schema]
    :return: list of authors information parsed to schema
    """

    authors_parsed = []

    for author in authors:
        # if only one affiliation, convert dict to list[dict]
        author_affiliations_ids = (
            [affiliation["@id"] for affiliation in author["affiliation"]]
            if isinstance(author["affiliation"], list)
            else [author["affiliation"]["@id"]]
        )

        author_affiliations = [
            affiliations[affiliation_id] for affiliation_id in author_affiliations_ids
        ]

        authors_parsed.append(
            ScopusAuthor(
                name=author.get_str("ce:given-name", default=None),
                surname=author["ce:surname"],
                indexed_name=author["ce:indexed-name"],
                initials=author["ce:initials"],
                order=int(author["@seq"]),
                affiliations=author_affiliations,
            )
        )

    return authors_parsed


def parse_scopus_abstract(response: benedict) -> ScopusAbstract:
    """
    Parse publication information obtained from Scopus Abstract Retrival API
    :param response: response obtained from API call
    :return: Information parsed to ScopusAbstract schema
    """

    # For cases with only one affiliation, convert the dict to a list of one item
    affiliations = (
        response["affiliation"]
        if isinstance(response["affiliation"], list)
        else [response["affiliation"]]
    )

    affiliations_parsed = parse_affiliations(affiliations)
    authors_with_affiliations = parse_authors(
        response["authors", "author"], affiliations_parsed
    )

    start_page = response.get_int(["coredata", "prism:startingPage"], default=None)
    end_page = response.get_int(["coredata", "prism:endingPage"], default=None)

    volume = response.get_str(["coredata", "prism:volume"], default=None)
    issue = response.get_str(
        ["item", "bibrecord", "head", "source", "volisspag", "voliss", "@issue"],
        default=None,
    )

    keywords = {
        keyword["$"].lower()
        for keyword in response.get_list(["authkeywords", "author-keyword"], default=[])
    }

    publication_date = response[
        "item", "bibrecord", "head", "source", "publicationdate"
    ]
    parsed_date = PublicationDate(
        year=publication_date.get_int("year"),
        month=publication_date.get_int("month", default=None),
        day=publication_date.get_int("day", default=None),
    )

    abstract = response.get_str(["coredata", "dc:description"], default=None)

    raw_issn_list = response.get_list(
        ["coredata", "prism:issn"], separator=" ", default=None
    )
    issn = [format_issn(issn) for issn in raw_issn_list] if raw_issn_list else None

    scopus_abstract = ScopusAbstract(
        abstract=abstract,
        journal=response["coredata", "prism:publicationName"].lower(),
        publisher=response["coredata", "dc:publisher"].lower(),
        document_type=response["coredata", "subtypeDescription"].lower(),
        title=response["coredata", "dc:title"],
        volume=volume,
        issue=issue,
        start_page=start_page,
        end_page=end_page,
        doi=response["coredata", "prism:doi"].lower(),
        keywords=keywords,
        date=parsed_date,
        authors=authors_with_affiliations,
        issn=issn,
    )

    return scopus_abstract


def get_scopus_error_code(
    status_code: int, response_data: dict
) -> ScopusAbstractRetrival:
    """
    Get error code from failed Scopus API call
    :param status_code: integer with the response code
    :param response_data: dictionary with the response
    :return: ScopusAbstractRetrival schema with error code but no information of publication
    """

    if status_code == 404:
        error_code = response_data["service-error"]["status"]["statusCode"]
        return ScopusAbstractRetrival(status_code=status_code, error_code=error_code)

    if status_code == 401:
        error_code = response_data["error-response"]["error-code"]
        return ScopusAbstractRetrival(status_code=status_code, error_code=error_code)

    return ScopusAbstractRetrival(status_code=400, error_code="UNEXPECTED_SCOPUS_ERROR")
