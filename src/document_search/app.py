"""
Module for document search API definition of sub application and endpoint
"""

from fastapi import FastAPI

from src.document_search.scopus.router import router

app = FastAPI()
app.include_router(router)
