"""
Application general settings
"""

from pydantic import BaseSettings, Field
from sqlalchemy import URL


class Settings(BaseSettings):
    """
    Class for general setting of project
    """

    # SDI database credentials
    db_driver = Field("mssql+pymssql", env="DB_DRIVER")
    db_user = Field("user", env="DB_USER")
    db_password = Field("pass", env="DB_PASSWORD")
    db_host = Field("host", env="DB_HOST")
    db_port = Field(1433, env="DB_PORT")
    db_database = Field("database", env="DB_DATABASE")

    # Scopus apikey
    scopus_key = Field("my_super_key", env="SCOPUS_KEY")

    class Config:
        """
        Dot env settings
        """

        case_sensitive = False
        env_file = ".env", ".env.prod"
        env_file_encoding = "utf-8"

    def get_db_url(self) -> URL:
        """
        Get database URL connection
        """
        return URL.create(
            self.db_driver,
            username=self.db_user,
            password=self.db_password,  # plain (unescaped) text
            host=self.db_host,
            port=self.db_port,
            database=self.db_database,
        )


settings = Settings()
