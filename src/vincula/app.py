"""
Module for vincula API definition of sub application and endpoints
"""


from fastapi import Depends, FastAPI

from src.vincula.dependencies import verify_apikey
from src.vincula.schemas import PublicationResponse
from src.vincula.service import get_publication

app = FastAPI(
    dependencies=[Depends(verify_apikey)],
)


@app.get("/test/{publication_id}", tags=["vincula"])
async def test(publication_id: int) -> PublicationResponse:
    """
    Endpoint for testing
    :param publication_id: id of publication inside database
    :return: Publication data
    """
    pub = get_publication(publication_id)
    return pub
