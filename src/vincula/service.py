"""
Module for business logic of vincula module
"""

from sqlalchemy import select

from database import sdi_db_conn
from src.vincula.schemas import PublicationResponse
from tables import Publication


def get_publication(pub_id: int) -> PublicationResponse:
    """
    Get publication information from database by id
    :param pub_id:  of publication in database
    :return: PublicationResponse schema with the publication data
    """
    stmt = select(Publication).where(Publication.id == pub_id)

    with sdi_db_conn.session() as session:
        result = session.execute(stmt).one()[0]

    pub = PublicationResponse(**result.as_dict())
    return pub
