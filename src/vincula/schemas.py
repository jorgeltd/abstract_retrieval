"""
Schema definitions for vincula module
"""

from typing import Union

from pydantic import BaseModel


class PublicationResponse(BaseModel):
    """
    Response schema with publication information from database
    """

    id: int
    nombre: str
    wos_uid: Union[str, None]
    doi: Union[str, None]
    nombre_revista: str
