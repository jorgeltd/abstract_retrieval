"""
Dependencies for vincula endpoints
"""

from typing import Annotated

from fastapi import Header, HTTPException


async def verify_apikey(x_apikey: Annotated[str, Header()]):
    """
    Verify the API key inside request header
    :param x_apikey: apikey string
    :return: If apikey is not valid, raises HTTPException
    """
    if x_apikey != "fake-super-secret-token":
        raise HTTPException(status_code=400, detail="X-Token header invalid")
