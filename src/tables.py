"""
Module for definition of mapped SDI database tables
using SQLAlchemy 2.0 declarative table with mapped_column()
"""

from sqlalchemy import inspect
from sqlalchemy.orm import Mapped, as_declarative, mapped_column

from database import sdi_db_conn


@as_declarative()
class Base:
    """
    Custom Base class for table mapping
    """

    def as_dict(self):
        """
        Return instance of class table as a dictionary
        """
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}


class Publication(Base):
    """
    Class map of 'publicaciones' table
    """

    __tablename__ = "publicaciones"
    __table_args__ = {"autoload_with": sdi_db_conn.engine}
    id: Mapped[int] = mapped_column(primary_key=True, nullable=False)
