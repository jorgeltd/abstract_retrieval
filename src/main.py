"""
FastAPI entry point
"""

from contextlib import asynccontextmanager

from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse

from constants import INTERNAL_SERVER_ERROR  # type: ignore
from database import sdi_db_conn  # type: ignore
from document_search.app import app as doc_search_app  # type: ignore
from vincula.app import app as vincula_app  # type: ignore

app = FastAPI()

# Mount sub apps
app.mount("/vincula", vincula_app)
app.mount("/document_search", doc_search_app)


# Middlewares
@vincula_app.middleware("http")
@doc_search_app.middleware("http")
@app.middleware("http")
async def exception_handling(request: Request, call_next):
    """
    Custom exception handler to catch global exception from endpoints
    """
    try:
        return await call_next(request)
    except Exception as error:
        print("Do some logging here")
        base_error_message = f"Failed to execute: {request.method}: {request.url}"
        return JSONResponse(
            status_code=500,
            content={
                "error_code": INTERNAL_SERVER_ERROR,
                "endpoint": base_error_message,
                "error": str(error),
            },
        )


@asynccontextmanager
async def lifespan(application: FastAPI):
    """
    Function to set up variables before startup and shutdown
    """
    yield
    sdi_db_conn.close()


@app.get("/")
async def root():
    """
    Test endpoint
    """
    return {"message": "Hello World"}
