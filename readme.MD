# SDI API

Project for custom API for SDI umag.

## Requirements

* pyenv
* poetry

## How to run

Set `.env` file inside root folder
```
# scopus vars
SCOPUS_KEY=scopuskey

# db vars
DB_DRIVER=mssql+pymssql 
DB_USER=user
DB_PASSWORD=pass
DB_HOST=some.host.com
DB_PORT=1234
DB_DATABASE=mydabase
```

First time environment setup
```shell
pyenv install python 3.9.17
pyenv local 3.9.17
poetry env use 3.9.17
poetry shell
poetry install --with=dev
```

After installing python and dependencies just run `poetry shell`

Run FastAPI
```shell
./run.sh
```
